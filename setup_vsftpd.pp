package { 'vsftpd' :
	ensure 		=> present,
}
file { 'vsftpd.conf':
	path 		=> '/etc/vsftpd.conf',
	owner 		=> 'root',
	group		=> 'root',
	mode		=> '0644'
}
service { 'vsftpd':
	name 		=> 'vsftpd',	
	enable  	=> true,
	hasstatus 	=> true,
	pattern 	=> 'vsftpd',
	manifest 	=> "
listen=YES
listen_port=21
dirmessage_enable=YES
use_localtime=YES
xferlog_enable=YES
connect_from_port_20=YES
secure_chroot_dir=/var/run/vsftpd/empty
rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
ssl_enable=NO
pam_service_name=vsftpd"
}
group {'user5':
	name 		=> 'user5',
	ensure		=> present,
	gid 		=> 2001,
}
user { 'user5':
	ensure 		=> present,
	home 		=> "/home/user5",
	shell		=> "/bin/bash",
	uid			=> 2001,
	gid 		=> 2001,
	password	=> '$6$/KkjqH2N$gwHe9X/HKbYHJ/00VJeRUIZvXAanj0LMM/81ioj7YDwsnTo.HKsTjRHNELq0h/KBLnFnUkEZoeWQcLtXSgp4q1' 
}
file { 'user5':
	path		=> '/home/user5',
	ensure 		=> directory,
	owner		=> user5,
	group		=> user5,
}

exec { 'lftp':
	command 	=> 'lftp ftp://user5:secret@localhost  -e "ls; pwd; bye"',
	path 		=> '/usr/bin',
	user 		=> 'user5',

}